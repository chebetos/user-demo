<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Chebetos\UserDemo\Container;
use Chebetos\UserDemo\FrontController;
use Chebetos\UserDemo\Controllers\UserController;
use Chebetos\UserDemo\Controllers\PageController;
use Chebetos\UserDemo\Model\UserRepository;
use Chebetos\UserDemo\Model\PhpSerializerFilePersistenceService;
use Chebetos\UserDemo\Dispatcher\SimpleDispatcherFactory;
use Chebetos\UserDemo\HTTP\RequestImpl;
use Chebetos\UserDemo\HTTP\ResponseImpl;
use Chebetos\UserDemo\Route\FileRouteRepository;
use Chebetos\UserDemo\Security\UserAuthenticationHandlerImpl;
use Chebetos\UserDemo\Security\SecurityHandlerImpl;
use Chebetos\UserDemo\ViewRenderer\ViewRendererImpl;

use Cascade\Cascade;

Cascade::fileConfig(__DIR__ . '/../resources/logging.config.yaml');
$logger = Cascade::getLogger('user-demo');

Container::set($logger, 'logger');

$filePath = __DIR__ . '/../resources/router-config.json';

$fileRouteRepository = new FileRouteRepository($filePath);

$sut = new SimpleDispatcherFactory($fileRouteRepository, $logger);
$dispatcher = $sut->getDispatcher();

$fileUserDB = __DIR__ . "/../resources/users.db";
$persistenceService = new PhpSerializerFilePersistenceService($fileUserDB);
$userRepository = new UserRepository($persistenceService);
$userController = new UserController($userRepository);

Container::set($userController);

$userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
$securityHandler = new SecurityHandlerImpl($userAuthenticationHandler);

$viewRenderer = new ViewRendererImpl();
$pageController = new PageController($viewRenderer, $userAuthenticationHandler);

Container::set($pageController);


$controller = new FrontController($dispatcher, $securityHandler);


$body = @file_get_contents('php://input');

$session = [];

if (PHP_SESSION_ACTIVE === session_status()) {
    $session = $_SESSION;
}

$request = new RequestImpl($_SERVER, $_REQUEST, $session, $body);
$response = new ResponseImpl();

$controller->handleRequest($request, $response);

$response->send();
