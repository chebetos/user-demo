<?php

namespace Chebetos\UserDemo\ViewRenderer;


class ViewRendererImplTest extends \PHPUnit_Framework_TestCase
{
    function testRenderPhpToString()
    {
        $file = __DIR__ . "/ViewExample.php";
        $vars = ['name' => 'world'];
        $viewRenderer = new ViewRendererImpl();
        $view = $viewRenderer->renderPhpToString($file, $vars);
        $this->assertEquals('<p>Hello world</p>', $view);
    }
}
