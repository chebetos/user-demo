<?php

namespace Chebetos\UserDemo;

use Chebetos\UserDemo\Controllers\UserController;

class ContainerTest extends \PHPUnit_Framework_TestCase
{

    function testGetNull()
    {
        $service = Container::get('name');
        $this->assertNull($service);
    }


    function testGetNewInstance()
    {
        $service = Container::get('\DateTime');
        $this->assertInstanceOf(\DateTime::class, $service);
    }


    function testSetAndGetOldInstance()
    {
        $date = new \DateTime();
        Container::set($date, 'date');
        $value = Container::get('date');
        $this->assertEquals($date, $value);
    }


    function testSetNotObject()
    {
        Container::set('string', 'sKey');
        $value = Container::get('sKey');
        $this->assertEquals(null, $value);
    }

    function testSetObject()
    {
        $date = new \DateTimeImmutable();
        Container::set($date);
        $value = Container::get('\DateTimeImmutable');
        $this->assertEquals($date, $value);
    }

    function testClearObject()
    {
        $date = new \DateTimeImmutable();
        Container::set($date, 'date');
        Container::clear('date');
        $value = Container::get('date');
        $this->assertNull($value);
    }
}
