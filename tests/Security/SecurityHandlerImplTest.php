<?php
/**
 * Created by PhpStorm.
 * User: cadelgado
 * Date: 16/06/16
 * Time: 18:03
 */

namespace Chebetos\UserDemo\Security;

use Chebetos\UserDemo\Route\RouteVO;
use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\Model\UserModel;


class SecurityHandlerImplTest extends \PHPUnit_Framework_TestCase
{
    public function testUserHasAuthorizationForAnonymous() {
        $userAuthHandler = $this->getMockBuilder(UserAuthenticationHandlerInterface::class)->getMock();

        $routeVO = new RouteVO(['GET'], ['/users'], [], ['role1', 'ANONYMOUS']);
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $securityHandler = new  SecurityHandlerImpl($userAuthHandler);
        $hasAuth = $securityHandler->userHasAuthorization($request, $routeVO);
        $this->assertEquals(SecurityHandlerImpl::OK, $hasAuth);
    }

    public function testUserHasAuthorizationUnauthorized() {
        $userAuthHandler = $this->getMockBuilder(UserAuthenticationHandlerInterface::class)->getMock();
        $userAuthHandler->method('getCurrentUser')->willReturn(null);

        $routeVO = new RouteVO(['GET'], ['/users'], [], ['role1']);
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $securityHandler = new SecurityHandlerImpl($userAuthHandler);
        $hasAuth = $securityHandler->userHasAuthorization($request, $routeVO);
        $this->assertEquals(SecurityHandlerImpl::UNAUTHORIZED, $hasAuth);
    }

    public function testUserHasAuthorizationLogged() {
        $user1 = new UserModel('username1', ['role1'], 'pass1');
        $userAuthHandler = $this->getMockBuilder(UserAuthenticationHandlerInterface::class)->getMock();
        $userAuthHandler->method('getCurrentUser')->willReturn($user1);

        $routeVO = new RouteVO(['GET'], ['/users'], [], [ SecurityHandlerInterface::LOGGED ]);
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $securityHandler = new SecurityHandlerImpl($userAuthHandler);
        $hasAuth = $securityHandler->userHasAuthorization($request, $routeVO);
        $this->assertEquals(SecurityHandlerImpl::OK, $hasAuth);
    }

    public function testUserHasAuthorizationSameRole() {
        $user1 = new UserModel('username1', ['role1'], 'pass1');
        $userAuthHandler = $this->getMockBuilder(UserAuthenticationHandlerInterface::class)->getMock();
        $userAuthHandler->method('getCurrentUser')->willReturn($user1);

        $routeVO = new RouteVO(['GET'], ['/users'], [], ['role1']);
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $securityHandler = new SecurityHandlerImpl($userAuthHandler);
        $hasAuth = $securityHandler->userHasAuthorization($request, $routeVO);
        $this->assertEquals(SecurityHandlerImpl::OK, $hasAuth);
    }

    public function testUserHasAuthorizationForbbidden() {
        $user1 = new UserModel('username1', ['role1'], 'pass1');
        $userAuthHandler = $this->getMockBuilder(UserAuthenticationHandlerInterface::class)->getMock();
        $userAuthHandler->method('getCurrentUser')->willReturn($user1);

        $routeVO = new RouteVO(['GET'], ['/users'], [], ['role2']);
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $securityHandler = new SecurityHandlerImpl($userAuthHandler);
        $hasAuth = $securityHandler->userHasAuthorization($request, $routeVO);
        $this->assertEquals(SecurityHandlerImpl::FORBIDDEN, $hasAuth);
    }
}
