<?php

namespace Chebetos\UserDemo\Security;

use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\Model\UserModel;
use Chebetos\UserDemo\Model\UserRepositoryInterface;

class UserAuthenticationHandlerImplTest extends \PHPUnit_Framework_TestCase
{
    public function testGetCurrentUserFromSession()
    {
        $user1 = new UserModel('username1', ['role1'], 'pass1');

        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $request->method('getSessionVarValue')->willReturn($user1);

        $userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
        $user = $userAuthenticationHandler->getCurrentUser($request);

        $this->assertEquals($user1->getUserName(), $user->getUsername());
    }

    public function testGetCurrentUserFromAuthUserAndPW()
    {
        $user1 = new UserModel('username1', ['role1'], 'pass1');

        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $request->method('getSessionVarValue')->willReturn(null);
        $request->method('getAuthUser')->willReturn('username1');
        $request->method('getAuthPassword')->willReturn('pass1');
        $userRepository->method('getUser')->willReturn($user1);

        $userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
        $user = $userAuthenticationHandler->getCurrentUser($request);

        $this->assertEquals($user1->getUserName(), $user->getUsername());
    }

    public function testGetCurrentUserEmptyAuthUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $request->method('getSessionVarValue')->willReturn(null);
        $request->method('getAuthUser')->willReturn(null);
        $request->method('getAuthPassword')->willReturn(null);

        $userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
        $user = $userAuthenticationHandler->getCurrentUser($request);

        $this->assertNull($user);
    }

    public function testGetCurrentUserEmptyAuthPW()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $request->method('getSessionVarValue')->willReturn(null);
        $request->method('getAuthUser')->willReturn("username1");
        $request->method('getAuthPassword')->willReturn(null);

        $userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
        $user = $userAuthenticationHandler->getCurrentUser($request);

        $this->assertNull($user);
    }

    public function testGetCurrentUserNotInRepo()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $request->method('getSessionVarValue')->willReturn(null);
        $request->method('getAuthUser')->willReturn("username1");
        $request->method('getAuthPassword')->willReturn("pw1");
        $userRepository->method('getUser')->willReturn(null);

        $userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
        $user = $userAuthenticationHandler->getCurrentUser($request);

        $this->assertNull($user);
    }

    public function testGetCurrentUserFromAuthUserAndInvalidPW()
    {
        $user1 = new UserModel('username1', ['role1'], 'pass1');

        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $request->method('getSessionVarValue')->willReturn(null);
        $request->method('getAuthUser')->willReturn('username1');
        $request->method('getAuthPassword')->willReturn('ps');
        $userRepository->method('getUser')->willReturn($user1);

        $userAuthenticationHandler = new UserAuthenticationHandlerImpl($userRepository);
        $user = $userAuthenticationHandler->getCurrentUser($request);

        $this->assertNull($user);
    }
}
