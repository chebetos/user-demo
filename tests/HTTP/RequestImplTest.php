<?php

namespace HTTP;

use Cascade\Cascade;
use Chebetos\UserDemo\HTTP\RequestImpl;
use Psr\Log\LoggerInterface;

class RequestImplTest extends \PHPUnit_Framework_TestCase
{
    public function testRequestImpl()
    {
        $server = [];
        $server['SERVER_PROTOCOL'] = "HTTP/1.1";
        $server['REQUEST_URI'] = "/index.php";
        $server['REQUEST_METHOD'] = "GET";
        $server['HTTP_HOST'] = "localhost:8000";
        $server['HTTP_CONNECTION'] = "keep-alive";
        $server['HTTP_PRAGMA'] = "no-cache";
        $server['HTTP_CACHE_CONTROL'] = "no-cache";
        $server['HTTP_UPGRADE_INSECURE_REQUESTS'] = 1;
        $server['HTTP_USER_AGENT'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36";
        $server['HTTP_ACCEPT'] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
        $server['HTTP_ACCEPT_ENCODING'] = "gzip, deflate, sdch";
        $server['HTTP_ACCEPT_LANGUAGE'] = "es,en;q=0.8";
        $server['HTTP_CONTENT_TYPE'] = "text/plain";
        $server['CONTENT_TYPE'] = "text/plain";
        $server['HTTP_CONTENT_LENGTH'] = "4";
        $server['CONTENT_LENGTH'] = "4";
        $server['PHP_AUTH_USER'] = "TEST_USER";
        $server['PHP_AUTH_PW'] = "TEST_PW";

        $body = "1234";
        $sut = new RequestImpl($server, ['param1' => 'param1Value' ], [ 'session1' => 'session1Value'], $body);

        $this->assertEquals('text/plain', $sut->getHeader('Content-Type'));
        $this->assertEquals(null, $sut->getHeader('unknown header'));
        $this->assertEquals('1234', $sut->getBody());
        $this->assertEquals('GET', $sut->getHttpMethod());
        $this->assertEquals('/index.php', $sut->getURI());
        $this->assertEquals('TEST_USER', $sut->getAuthUser());
        $this->assertEquals('TEST_PW', $sut->getAuthPassword());

        $this->assertEquals('param1Value', $sut->getParam('param1'));
        $this->assertEquals('session1Value', $sut->getSessionVarValue('session1'));

        $this->assertNull($sut->getParam('param'));
        $this->assertNull($sut->getSessionVarValue('param'));
    }
}
