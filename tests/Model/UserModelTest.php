<?php

namespace Chebetos\UserDemo\Model;

class UserModelTest extends \PHPUnit_Framework_TestCase
{
    function testRoles()
    {
        $user = new UserModel('user1');
        $user->addRole('user1');
        $this->assertEquals(true, $user->isUserInRole('user1'));
        $this->assertContains('USER1', $user->getRoles());
    }

    function testCheckPassword()
    {
        $user = new UserModel('user1', ['role1'], 'pass');
        $user->addRole('user1');
        $this->assertEquals(true, $user->checkPassword('pass'));
        $this->assertContains('ROLE1', $user->getRoles());
    }
}
