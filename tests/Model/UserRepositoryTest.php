<?php

namespace Chebetos\UserDemo\Model;

class UserRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct()
    {
        $persistenceService = $this->getMockBuilder(PersistenceServiceInterface::class)->getMock();

        $user = new UserModel('username1', ['role1'], 'pass1');
        $persistenceService->method('getElements')->willReturn([$user]);
        $persistenceService->method('getElement')->willReturn($user);
        $this->userRepository = new UserRepository($persistenceService);
    }

    function testGetUsers()
    {
        $data = $this->userRepository->getUsers();
        $this->assertNotEmpty($data);
        $this->assertInstanceOf(UserModel::class, $data[0]);
    }

    function testGetUser()
    {
        $data = $this->userRepository->getUser('username1');
        $this->assertNotEmpty($data);
        $this->assertInstanceOf(UserModel::class, $data);
    }

    function testDeleteAndSaveUser()
    {
        $this->userRepository->deleteUser('username1');

        $data = $this->userRepository->getUsers();
        $this->assertCount(1, $data);

        $user = new UserModel('username1', ['role1'], 'pass1');
        $this->userRepository->saveUser($user);

        $filename = sys_get_temp_dir() . '/userrepo.test';
        $persistenceService = new PhpSerializerFilePersistenceService($filename);
        $newUserRepository = new UserRepository($persistenceService);

        $data = $newUserRepository->getUsers();
        $this->assertNotEmpty($data);
        $this->assertInstanceOf(UserModel::class, $data[0]);

    }

    function testConstruct()
    {
        $filename = sys_get_temp_dir() . '/userrepo1.test';
        $persistenceService = new PhpSerializerFilePersistenceService($filename);
        $newUserRepository = new UserRepository($persistenceService);
        $this->assertInstanceOf(UserRepository::class, $newUserRepository);
    }
}
