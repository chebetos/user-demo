<?php

namespace Chebetos\UserDemo\Model;


class PhpSerializerFilePersistenceServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PhpSerializerFilePersistenceService
     */
    private $persistenceService;

    private $filename;

    public function __construct()
    {
        $this->filename = sys_get_temp_dir() . '/serializerservice.test';
        $this->persistenceService = new PhpSerializerFilePersistenceService($this->filename);

        $data = new \stdClass();
        $data->clave = 'clave';
        $data->valor = 'valor';
        $this->persistenceService->saveElement('clave', $data);
    }

    function testLoad() {
        $otherPersistenceService = new PhpSerializerFilePersistenceService($this->filename);
        $otherPersistenceService->load();

        $dataStorage = $otherPersistenceService->getElements();
        $this->assertNotEmpty($dataStorage);
    }

    function testLoadEmptyFile() {
        $otherPersistenceService = new PhpSerializerFilePersistenceService(sys_get_temp_dir() . '/newserializerservice.test');
        $result = $otherPersistenceService->load();
        $this->assertFalse($result);
        
        $dataStorage = $otherPersistenceService->getElements();
        $this->assertEmpty($dataStorage);
    }

    function testGetElements()
    {
        $dataStorage = $this->persistenceService->getElements();
        $this->assertNotEmpty($dataStorage);
    }

    function testGetElement()
    {
        $data = $this->persistenceService->getElement('clave');
        $this->assertEquals('valor', $data->valor);
    }

    function testDeleteElement()
    {
        $this->persistenceService->deleteElement('clave');

        $dataStorage = $this->persistenceService->getElements();
        $this->assertEmpty($dataStorage);

        $data = new \stdClass();
        $data->clave = 'clave';
        $data->valor = 'valor';
        $this->persistenceService->saveElement('clave', $data);
    }
}
