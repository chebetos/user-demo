<?php

namespace Chebetos\UserDemo;

use Cascade\Cascade;
use Chebetos\UserDemo\Controllers\UserController;
use Chebetos\UserDemo\Route\FileRouteRepository;
use Chebetos\UserDemo\Dispatcher\SimpleDispatcherFactory;
use Chebetos\UserDemo\Model\PhpSerializerFilePersistenceService;
use Chebetos\UserDemo\Model\UserRepository;
use Chebetos\UserDemo\HTTP\RequestImpl;
use Chebetos\UserDemo\HTTP\ResponseImpl;
use Chebetos\UserDemo\Security\SecurityHandlerInterface;
use Psr\Log\LoggerInterface;

class FrontControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FrontController
     */
    private $controller;

    /**
     * @var array
     */
    private $serverData;

    public function __construct()
    {
        Cascade::fileConfig('resources/logging.config.yaml');
        $this->logger = Cascade::getLogger(self::class);
        Container::set($this->logger, 'logger');

        $filename = sys_get_temp_dir() . '/userrepo.test';

        $persistenceService = new PhpSerializerFilePersistenceService($filename);
        $userRepository = new UserRepository($persistenceService);

        $userController = new UserController($userRepository);

        Container::set($userRepository);

        Container::set($userController);

        $filePath = 'resources/router-config.json';
        
        $fileRouteRepository = new FileRouteRepository($filePath);

        $sut = new SimpleDispatcherFactory($fileRouteRepository);
        $dispatcher = $sut->getDispatcher();

        $securityHandler = $this->getMockBuilder(SecurityHandlerInterface::class)->getMock();
        $securityHandler->method('userHasAuthorization')->willReturn(SecurityHandlerInterface::OK);

        $this->controller = new FrontController($dispatcher, $securityHandler);

        $this->serverData = [];
        $this->serverData['SERVER_PROTOCOL'] = "HTTP/1.1";
        $this->serverData['HTTP_HOST'] = "localhost:8000";
        $this->serverData['HTTP_CONNECTION'] = "keep-alive";
        $this->serverData['HTTP_PRAGMA'] = "no-cache";
        $this->serverData['HTTP_CACHE_CONTROL'] = "no-cache";
        $this->serverData['HTTP_UPGRADE_INSECURE_REQUESTS'] = 1;
        $this->serverData['HTTP_USER_AGENT'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36";
        $this->serverData['HTTP_ACCEPT'] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
        $this->serverData['HTTP_ACCEPT_ENCODING'] = "gzip, deflate, sdch";
        $this->serverData['HTTP_ACCEPT_LANGUAGE'] = "es,en;q=0.8";
        $this->serverData['PHP_AUTH_USER'] = "TEST_USER";
        $this->serverData['PHP_AUTH_PW'] = "TEST_PW";

    }

    function testGetUsers()
    {
        $server = array();
        $server['REQUEST_URI'] = "/users";
        $server['REQUEST_METHOD'] = "GET";
        $server = array_merge($this->serverData, $server);

        $request = new RequestImpl($server, [], [], null);

        $response = new ResponseImpl();
        $this->controller->handleRequest($request, $response);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getStatusText());
    }

    function testPostUsers()
    {
        $body = "{\"username\":\"user1\",\"roles\":[\"ADMIN\"],\"password\":\"user1\"}";

        $server = array();
        $server['REQUEST_URI'] = "/users";
        $server['REQUEST_METHOD'] = "POST";

        $server['HTTP_CONTENT_TYPE'] = "text/plain";
        $server['HTTP_CONTENT_LENGTH'] = sizeof($body);
        $server['CONTENT_TYPE'] = $server['HTTP_CONTENT_TYPE'];
        $server['CONTENT_LENGTH'] = $server['HTTP_CONTENT_LENGTH'];

        $server = array_merge($this->serverData, $server);

        $request = new RequestImpl($server, [], [], $body);

        $response = new ResponseImpl();
        $this->controller->handleRequest($request, $response);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('CREATED', $response->getStatusText());
    }

    function testMethodNotAllowed()
    {
        $body = "1234";

        $server = array();
        $server['REQUEST_URI'] = "/page1";
        $server['REQUEST_METHOD'] = "POST";

        $server['HTTP_CONTENT_TYPE'] = "text/plain";
        $server['HTTP_CONTENT_LENGTH'] = sizeof($body);
        $server['CONTENT_TYPE'] = $server['HTTP_CONTENT_TYPE'];
        $server['CONTENT_LENGTH'] = $server['HTTP_CONTENT_LENGTH'];

        $server = array_merge($this->serverData, $server);

        $request = new RequestImpl($server, [], [], $body);

        $response = new ResponseImpl();
        $this->controller->handleRequest($request, $response);

        $this->assertEquals(405, $response->getStatusCode());
        $this->assertEquals('METHOD NOT ALLOWED', $response->getStatusText());
        $this->assertContains('GET', $response->getHeader('Allow'));
    }

    function testNotFound()
    {
        $body = "1234";

        $server = array();
        $server['REQUEST_URI'] = "/pageNotRouted";
        $server['REQUEST_METHOD'] = "GET";

        $server['HTTP_CONTENT_TYPE'] = "text/plain";
        $server['HTTP_CONTENT_LENGTH'] = sizeof($body);
        $server['CONTENT_TYPE'] = $server['HTTP_CONTENT_TYPE'];
        $server['CONTENT_LENGTH'] = $server['HTTP_CONTENT_LENGTH'];

        $server = array_merge($this->serverData, $server);

        $request = new RequestImpl($server, [], [], $body);

        $response = new ResponseImpl();
        $this->controller->handleRequest($request, $response);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
    }
}