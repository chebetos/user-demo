<?php

namespace Chebetos\UserDemo\Route;

use Cascade\Cascade;
use Psr\Log\LoggerInterface;

class FileRouteRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct()
    {
        Cascade::fileConfig('resources/logging.config.yaml');
        $this->logger = Cascade::getLogger(self::class);
    }

    public function testLoadRoutes()
    {
        $sut = new FileRouteRepository('resources/router-config.json');
        $routes = $sut->loadRoutes();

        $this->assertContains('GET', $routes[0]->getHttpMethod());

        $this->assertEquals('/users', $routes[0]->getRoutePattern());

        $this->assertEquals('Chebetos\UserDemo\Controllers\UserController', $routes[0]->getHandler()[0]);
        $this->assertEquals('getUsers', $routes[0]->getHandler()[1]);

        $this->assertContains('ADMIN', $routes[0]->getRoles());
    }

    public function message() {
        $this->logger->info("print message");
    }
}