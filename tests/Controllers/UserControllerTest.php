<?php

namespace Chebetos\UserDemo\Controllers;


use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\HTTP\ResponseImpl;
use Chebetos\UserDemo\Model\UserModel;
use Chebetos\UserDemo\Model\UserRepositoryInterface;

class UserControllerTest extends \PHPUnit_Framework_TestCase
{
    function testGetUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $user = new UserModel('username1', ['role1'], 'pass1');
        $userRepository->method('getUsers')->willReturn([$user]);
        $userRepository->method('getUser')->willReturn($user);
        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->getUser($request, $response, $params);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', $response->getStatusText());
        $this->assertContains('application/json', $response->getHeader('Content-Type'));
        $this->assertEquals(json_encode($user), $response->getBody());
    }

    function testGetUnexistingUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $userRepository->method('getUser')->willReturn(null);
        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->getUser($request, $response, $params);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
        $this->assertContains('application/json', $response->getHeader('Content-Type'));
        $this->assertEquals('{"message":"username1 NOT FOUND"}', $response->getBody());
    }

    function testPostUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)->getMock();

        $userRepository->method('getUser')->willReturn(null);
        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->getUser($request, $response, $params);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
        $this->assertContains('application/json', $response->getHeader('Content-Type'));
        $this->assertEquals('{"message":"username1 NOT FOUND"}', $response->getBody());
    }

    function testPutUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->setMethods(['deleteUser', 'getUser', 'getUsers', 'saveUser'])
            ->getMock();

        $user = new UserModel('username1', ['role1'], 'pass1');

        $userRepository->expects($this->once())
            ->method('saveUser')
            ->with($this->equalTo($user));

        $userRepository->method('getUser')->willReturn($user);

        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->method('getBody')->willReturn('{ "username": "username1", "password": "pass1", "roles": ["ROLE1"] }');

        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->putUser($request, $response, $params);

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('NO CONTENT', $response->getStatusText());
    }

    function testPutUserWithoutUserName()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->setMethods(['deleteUser', 'getUser', 'getUsers', 'saveUser'])
            ->getMock();

        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->method('getBody')->willReturn('{ "username": "username1", "password": "pass1", "roles": ["ROLE1"] }');

        $response = new ResponseImpl();
        $params = ['username' => ''];

        $userController->putUser($request, $response, $params);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
    }

    function testPutUnexistingUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->setMethods(['deleteUser', 'getUser', 'getUsers', 'saveUser'])
            ->getMock();

        $userRepository->method('getUser')->willReturn(null);

        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->method('getBody')->willReturn('{ "username": "username1", "password": "pass1", "roles": ["ROLE1"] }');

        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->putUser($request, $response, $params);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
    }

    function testDeleteUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->setMethods(['deleteUser', 'getUser', 'getUsers', 'saveUser'])
            ->getMock();

        $user = new UserModel('username1', ['role1'], 'pass1');

        $userRepository->expects($this->once())
            ->method('deleteUser')
            ->with($this->equalTo('username1'));

        $userRepository->method('getUser')->willReturn($user);

        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->deleteUser($request, $response, $params);

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('NO CONTENT', $response->getStatusText());
    }

    function testDeleteUserWithoutUserName()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->setMethods(['deleteUser', 'getUser', 'getUsers', 'saveUser'])
            ->getMock();

        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $response = new ResponseImpl();
        $params = ['username' => ''];

        $userController->deleteUser($request, $response, $params);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
    }

    function testDeleteUnexistingUser()
    {
        $userRepository = $this->getMockBuilder(UserRepositoryInterface::class)
            ->setMethods(['deleteUser', 'getUser', 'getUsers', 'saveUser'])
            ->getMock();

        $userRepository->method('getUser')->willReturn(null);

        $userController = new UserController($userRepository);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $response = new ResponseImpl();
        $params = ['username' => 'username1'];

        $userController->deleteUser($request, $response, $params);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('NOT FOUND', $response->getStatusText());
    }
}
