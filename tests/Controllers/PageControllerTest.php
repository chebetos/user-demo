<?php

namespace Chebetos\UserDemo\Controllers\PageController;

use Chebetos\UserDemo\Controllers\PageController;
use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\HTTP\ResponseImpl;
use Chebetos\UserDemo\Model\UserModel;
use Chebetos\UserDemo\Security\UserAuthenticationHandlerInterface;
use Chebetos\UserDemo\ViewRenderer\ViewRendererInterface;


class PageControllerTest extends \PHPUnit_Framework_TestCase
{

    function testPageView()
    {
        $viewRenderer = $this->getMockBuilder(ViewRendererInterface::class)->getMock();
        $viewRenderer->method('renderPhpToString')->willReturn('Hola PAGE1');

        $user = new UserModel('PAGE1');
        $authenticationHandler = $this->getMockBuilder(UserAuthenticationHandlerInterface::class)->getMock();
        $authenticationHandler->method('getCurrentUser')->willReturn($user);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $response = new ResponseImpl();

        $pageController = new PageController($viewRenderer, $authenticationHandler);
        $pageController->pageView($request, $response, []);

        $this->assertEquals('Hola PAGE1', $response->getBody());
    }

}
