<?php

namespace Chebetos\UserDemo\Dispatcher;

use Cascade\Cascade;
use Chebetos\UserDemo\Route\FileRouteRepository;
use Psr\Log\LoggerInterface;

class DispatcherFactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var LoggerInterface
     */
    private $logger;
    
    public function __construct()
    {
        Cascade::fileConfig('resources/logging.config.yaml');
        $this->logger = Cascade::getLogger(self::class);
    }
    
    public function testSimpleDispatcherFactory() 
    {
        $filePath = 'resources/router-config.json';
        $fileRouteRepository = new FileRouteRepository($filePath);


        $sut = new SimpleDispatcherFactory($fileRouteRepository, $this->logger);
        $dispatcher = $sut->getDispatcher();
        $this->assertInstanceOf('FastRoute\Dispatcher', $dispatcher);
    }
}