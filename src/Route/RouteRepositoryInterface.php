<?php

namespace Chebetos\UserDemo\Route;

interface RouteRepositoryInterface
{
    /**
     * @return RouteContainerVO
     */
    function loadRoutes();
}