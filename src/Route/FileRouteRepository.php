<?php

namespace Chebetos\UserDemo\Route;

class FileRouteRepository implements RouteRepositoryInterface
{

    /**
     * @var string
     */
    private $filePath;

    /**
     * FileRouteRepository constructor.
     * @param string $filePathParam
     */
    public function __construct($filePathParam)
    {
        $this->filePath = $filePathParam;
    }

    /**
     * @return RouteContainerVO
     */
    function loadRoutes()
    {
        $routes = array();
        $json = file_get_contents($this->filePath);

        $data = json_decode($json, true);
        foreach ($data as $route)
        {
            $routeVO = new RouteVO($route['http_method'], $route['route_pattern'], $route['handler'], $route['roles']);
            $routes[] = $routeVO;
        }

        return $routes;
    }
}