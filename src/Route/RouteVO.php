<?php

namespace Chebetos\UserDemo\Route;

class RouteVO implements \JsonSerializable
{

    /**
     * @var array
     */
    private $httpMethod;

    /**
     * @var string
     */
    private $routePattern;

    /**
     * @var string | array
     */
    private $handler;

    /**
     * @var array
     */
    private $roles;

    /**
     * RouteVO constructor.
     * 
     * @param array $httpMethodP
     * @param string $routePatternP
     * @param string | array $handlerP
     * @param array $rolesP
     */
    public function __construct(array $httpMethodP, $routePatternP, $handlerP, array $rolesP) {
        $this->httpMethod = $httpMethodP;
        $this->routePattern = $routePatternP;
        $this->handler = $handlerP;
        $this->roles = [];
        foreach ($rolesP as $role) {
            $this->roles[] = strtoupper($role);
        }
    }

    /**
     * @return array
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @return string
     */
    public function getRoutePattern()
    {
        return $this->routePattern;
    }

    /**
     * @return string | array
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}