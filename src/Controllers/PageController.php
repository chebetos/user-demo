<?php

namespace Chebetos\UserDemo\Controllers;

use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\HTTP\ResponseInterface;
use Chebetos\UserDemo\Security\UserAuthenticationHandlerInterface;
use Chebetos\UserDemo\ViewRenderer\ViewRendererInterface;

class PageController
{

    const TEMPLATE = __DIR__ . "/PageTemplate.php";
    /**
     * @var ViewRendererInterface
     */
    private $viewRenderer;

    /**
     * @var UserAuthenticationHandlerInterface
     */
    private $userAuthenticationHandler;

    /**
     * PageController constructor.
     * @param ViewRendererInterface $viewRenderer
     * @param UserAuthenticationHandlerInterface $authenticationHandler
     */
    public function __construct(ViewRendererInterface $viewRenderer, UserAuthenticationHandlerInterface $authenticationHandler)
    {
        $this->viewRenderer = $viewRenderer;
        $this->userAuthenticationHandler = $authenticationHandler;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param $params
     */
    function pageView(RequestInterface $request, ResponseInterface $response, $params)
    {
        $user = $this->userAuthenticationHandler->getCurrentUser($request);
        $username = $user->getUsername();
        $body = $this->pageRender($username);
        $response->setStatus(200, 'OK');
        $response->setBody($body, "text/html");
    }

    private function pageRender($userName)
    {
        $file = __DIR__ . "/PageTemplate.php";
        $vars = ['name' => $userName];
        return $this->viewRenderer->renderPhpToString($file, $vars);
    }
}