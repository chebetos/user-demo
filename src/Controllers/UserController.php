<?php

namespace Chebetos\UserDemo\Controllers;

use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\HTTP\ResponseInterface;
use Chebetos\UserDemo\Model\UserModel;
use Chebetos\UserDemo\Model\UserRepositoryInterface;

class UserController
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param $params
     */
    function getUser(RequestInterface $request, ResponseInterface $response, $params)
    {
        $username = $params['username'];
        $user = $this->userRepository->getUser($username);
        if (empty($user)) {
            $this->prepare404NotFound($response, $username);
            return;
        }
        $response->setStatus(200, 'OK');
        $response->setBody(json_encode($user), "application/json");
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param $params
     */
    function getUsers(RequestInterface $request, ResponseInterface $response, $params)
    {
        $users = $this->userRepository->getUsers();
        $response->setStatus(200, 'OK');
        $response->setBody(json_encode($users), "application/json");
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param $params
     */
    function postUser(RequestInterface $request, ResponseInterface $response, $params)
    {
        $body = $request->getBody();
        $user = $this->userFromRequestBody($body);
        $this->userRepository->saveUser($user);
        $response->setStatus(201, 'CREATED');
        $response->setHeader('Location', "/users/" . $user->getUsername());
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param $params
     */
    function deleteUser(RequestInterface $request, ResponseInterface $response, $params)
    {
        $username = $params['username'];
        if (empty($username)) {
            $this->prepare404NotFound($response, $username);
            return;
        }
        $user = $this->userRepository->getUser($username);
        if (empty($user)) {
            $this->prepare404NotFound($response, $username);
            return;
        }
        $this->userRepository->deleteUser($username);
        $response->setStatus(204, 'NO CONTENT');
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param $params
     */
    function putUser(RequestInterface $request, ResponseInterface $response, $params)
    {
        $username = $params['username'];
        if (empty($username)) {
            $this->prepare404NotFound($response, $username);
            return;
        }
        $user = $this->userRepository->getUser($username);
        if (empty($user)) {
            $this->prepare404NotFound($response, $username);
            return;
        }
        $body = $request->getBody();
        $user = $this->userFromRequestBody($body);
        $this->userRepository->saveUser($user);
        $response->setStatus(204, 'NO CONTENT');
        $response->setHeader('Location', "/users/" . $user->getUsername());
    }

    /**
     * @param ResponseInterface $response
     * @param $username
     */
    public function prepare404NotFound(ResponseInterface $response, $username)
    {
        $response->setStatus(404, 'NOT FOUND');
        $message = [
            'message' => "$username NOT FOUND"
        ];
        $response->setBody(json_encode($message), "application/json");
    }

    /**
     * @param $body
     * @return UserModel
     */
    public function userFromRequestBody($body)
    {
        $stdUser = json_decode($body);
        $user = new UserModel(
            $stdUser->username,
            $stdUser->roles,
            $stdUser->password
        );
        return $user;
    }
}