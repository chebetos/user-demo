<?php

namespace Chebetos\UserDemo\Dispatcher;

use Chebetos\UserDemo\Route\RouteRepositoryInterface;
use FastRoute\RouteCollector;
use FastRoute\Dispatcher;
use Psr\Log\LoggerInterface;


class SimpleDispatcherFactory extends AbstractDispatcherFactory
{

    /**
     * SimpleDispatcherFactory constructor.
     * @param RouteRepositoryInterface $routeRepositoryParam
     */
    public function __construct(RouteRepositoryInterface $routeRepositoryParam)
    {
        parent::__construct($routeRepositoryParam);
    }

    /**
     * @return Dispatcher
     */
    function getDispatcher()
    {
        $dispatcher = \FastRoute\simpleDispatcher(function(RouteCollector $r)
        {
            SimpleDispatcherFactory::setupDispatcher($r);
        });
        return $dispatcher;
    }
}