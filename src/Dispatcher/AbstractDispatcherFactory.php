<?php

namespace Chebetos\UserDemo\Dispatcher;

use Chebetos\UserDemo\Container;
use Chebetos\UserDemo\Route\RouteRepositoryInterface;
use FastRoute\RouteCollector;
use FastRoute\Dispatcher;
use Psr\Log\LoggerInterface;

abstract class AbstractDispatcherFactory {

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var RouteRepositoryInterface
     *
     */
    private $routeRepository;

    /**
     * AbstractDispatcherFactory constructor.
     *
     * @param RouteRepositoryInterface $routeRepositoryParam
     */
    public function __construct(RouteRepositoryInterface $routeRepositoryParam) {
        $this->routeRepository = $routeRepositoryParam;
        $this->logger = Container::get('logger');
    }

    /**
     * @param RouteCollector $r
     */
    protected function setupDispatcher(RouteCollector $r) {
        $routes = $this->routeRepository->loadRoutes();
        foreach ($routes as $route) {
            $r->addRoute($route->getHttpMethod(), $route->getRoutePattern(), $route);
        }
    }

    abstract function getDispatcher();
}