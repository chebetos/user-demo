<?php

namespace Chebetos\UserDemo;


class Container
{

    /**
     * @var array
     */
    private static $instances = array();

    /**
     * @param string $name
     * @return object
     */
    static function get($name)
    {
        if (empty(self::$instances[$name]))
        {
            if (class_exists($name)) {
                self::$instances[$name] = new $name();
            } else {
                return null;
            }
        }
        return self::$instances[$name];
    }

    /**
     * @param object $instance
     * @param string $name
     */
    static function set($instance, $name = null)
    {
        if (!is_object($instance)) {
            return;
        }
        if (empty($name)) {
            $name = get_class($instance);
        }
        self::$instances[$name] = $instance;
    }

    static function clear($name)
    {
        unset(self::$instances[$name]);
    }
}