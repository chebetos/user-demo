<?php

namespace Chebetos\UserDemo\Model;


interface UserRepositoryInterface
{
    /**
     * @return array
     */
    function getUsers();

    /**
     * @param $username
     * @return UserModel
     */
    function getUser($username);

    /**
     * @param UserModel $user
     */
    function saveUser(UserModel $user);

    /**
     * @param $username
     */
    function deleteUser($username);
}