<?php

namespace Chebetos\UserDemo\Model;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var PersistenceServiceInterface
     */
    private $persistenceService;

    /**
     * UserRepository constructor.
     *
     * @param PersistenceServiceInterface $persistenceService
     */
    public function __construct(PersistenceServiceInterface $persistenceService)
    {
        $this->persistenceService = $persistenceService;
        $this->persistenceService->load();
        if (empty($this->persistenceService->getElements())) {
            $defaultUser = new UserModel('admin', ['ADMIN'], 'admin');
            $this->saveUser($defaultUser);
        }
    }

    public function __destruct() {
        $this->persistenceService->save();
    }

    /**
     * @return array
     */
    function getUsers()
    {
        return $this->persistenceService->getElements();
    }

    /**
     * @param UserModel $user
     * @return bool
     */
    function saveUser(UserModel $user)
    {
        return $this->persistenceService->saveElement($user->getUsername(), $user);
    }

    /**
     * @param $username
     */
    function deleteUser($username)
    {
        $this->persistenceService->deleteElement($username);
        $this->persistenceService->save();
    }

    /**
     * @param $username
     * @return UserModel
     */
    function getUser($username)
    {
        return $this->persistenceService->getElement($username);
    }
}