<?php

namespace Chebetos\UserDemo\Model;


class UserModel implements \JsonSerializable
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var array roles
     */
    private $roles;

    /**
     * @var string
     */
    private $password;

    /**
     * UserModel constructor.
     *
     * @param $username
     * @param array $roles
     * @param string $password
     */
    public function __construct($username, array $roles = array(), $password = "")
    {
        $upperRoles = [];
        foreach ($roles as $role) {
            $upperRoles[] = strtoupper($role);
        }
        $this->username = $username;
        $this->roles = $upperRoles;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $passwordToCheck
     * @return bool
     */
    public function checkPassword($passwordToCheck)
    {
        return ($this->password === $passwordToCheck);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param $role
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if (empty($this->roles))
        {
            $this->roles = array();
        }
        if (!$this->isUserInRole($role)) {
            $this->roles[] = $role;
        }
    }

    /**
     * @param $role
     * @return bool
     */
    public function isUserInRole($role)
    {
        $role = strtoupper($role);
        return in_array($role, $this->roles);
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);
        unset($data['password']);
        return $data;
    }
}