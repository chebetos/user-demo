<?php

namespace Chebetos\UserDemo\Model;

/**
 * Interface PersistenceServiceInterface.
 * @package Chebetos\UserDemo\Model
 */
interface PersistenceServiceInterface
{
    /**
     * @return boolean
     */
    function save();

    /**
     * @return boolean
     */
    function load();

    /**
     * @return array
     */
    function getElements();

    /**
     * @param string $key
     * @return object
     */
    function getElement($key);

    /**
     * @param string $key
     * @return boolean
     */
    function deleteElement($key);

    /**
     * @param string $key
     * @param object $element
     * @return boolean
     */
    function saveElement($key, $element);
}