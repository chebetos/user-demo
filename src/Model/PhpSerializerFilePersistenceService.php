<?php

namespace Chebetos\UserDemo\Model;

class PhpSerializerFilePersistenceService implements PersistenceServiceInterface
{

    /**
     * @var array
     */
    private $dataStorage = array();

    /**
     * @var string
     */
    private $fileStorage;

    /**
     * PhpSerializerFilePersistenceService constructor.
     * @param string $filename
     */
    public function __construct($filename) {
        $this->fileStorage = $filename;
    }

    /**
     * @return boolean
     */
    function save()
    {
        $dataString = serialize($this->dataStorage);
        return file_put_contents($this->fileStorage, $dataString);
    }

    /**
     * @return boolean
     */
    function load()
    {
        if (!file_exists($this->fileStorage)) {
            return false;
        }
        $dataString = file_get_contents($this->fileStorage);
        $this->dataStorage = unserialize($dataString);
        return true;
    }

    /**
     * @return array
     */
    function getElements()
    {
        return array_values($this->dataStorage);
    }

    /**
     * @param string $key
     * @return object
     */
    function getElement($key)
    {
        if (isset($this->dataStorage[$key]))
        {
            return $this->dataStorage[$key];
        }
        return null;
    }

    /**
     * @param string $key
     * @return boolean
     */
    function deleteElement($key)
    {
        unset($this->dataStorage[$key]);
        return $this->save();
    }

    /**
     * @param string $key
     * @param object $element
     * @return boolean
     */
    function saveElement($key, $element)
    {
        $this->dataStorage[$key] = $element;
        return $this->save();
    }
}