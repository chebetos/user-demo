<?php

namespace Chebetos\UserDemo\HTTP;


class RequestImpl extends MessageImpl implements RequestInterface
{

    /**
     * @var string
     */
    protected $httpMethod;

    /**
     * @var string
     */
    protected $uri;

    /**
     * @var string
     */
    protected $authUser;

    /**
     * @var string
     */
    protected $authPassword;

    /**
     * @var array
     */
    private $requestParams;

    /**
     * @var array
     */
    private $sessionVars;

    public function __construct(array $server, array $requestParams, array $session, $body = null)
    {
        $headers = [];
        foreach ($server as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
                $headers[$name] = $value;
            } else if ($name == "CONTENT_TYPE") {
                $headers["Content-Type"] = $value;
            } else if ($name == "CONTENT_LENGTH") {
                $headers["Content-Length"] = $value;
            }
        }
        $this->headers = $headers;

        $this->httpMethod = $server['REQUEST_METHOD'];

        $uri = $server['REQUEST_URI'];
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $this->uri = rawurldecode($uri);

        if (isset($server['PHP_AUTH_USER'])) {
            $this->authUser = $server['PHP_AUTH_USER'];
        }

        if (isset($server['PHP_AUTH_PW'])) {
            $this->authPassword = $server['PHP_AUTH_PW'];
        }

        $this->body = $body;

        $this->requestParams = $requestParams;

        $this->sessionVars = $session;
    }
    
    /**
     * @return string
     */
    function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @return string
     */
    function getURI()
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    function getAuthUser()
    {
        return $this->authUser;
    }

    /**
     * @return string
     */
    function getAuthPassword()
    {
        return $this->authPassword;
    }

    function getParam($paramName)
    {
        if (empty($paramName) || !array_key_exists($paramName, $this->requestParams))
        {
            return null;
        }
        return $this->requestParams[$paramName];
    }

    function getSessionVarValue($paramName)
    {
        if (empty($paramName) || !array_key_exists($paramName, $this->sessionVars))
        {
            return null;
        }
        return $this->sessionVars[$paramName];
    }
}