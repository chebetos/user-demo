<?php

namespace Chebetos\UserDemo\HTTP;


interface RequestInterface extends MessageInterface
{
    /**
     * @return string
     */
    function getHttpMethod();

    /**
     * @return string
     */
    function getURI();

    /**
     * @return string
     */
    function getAuthUser();

    /**
     * @return string
     */
    function getAuthPassword();

    /**
     * @param $paramName
     * @return string
     */
    function getParam($paramName);

    function getSessionVarValue($sessionVar);
}