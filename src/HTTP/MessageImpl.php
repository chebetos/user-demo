<?php

namespace Chebetos\UserDemo\HTTP;


class MessageImpl implements MessageInterface
{
    /**
     * @var array
     */
    protected $headers;

    /**
     * @var string
     */
    protected $body;

    /**
     * @return array
     */
    function getHeaders()
    {

        return $this->headers;
    }

    /**
     * @param $headerName
     * @return string
     */
    function getHeader($headerName)
    {
        $headersCopy = array_change_key_case($this->getHeaders(), CASE_LOWER);
        $toLowerHeaderName = strtolower($headerName);
        if (array_key_exists($toLowerHeaderName, $headersCopy)) {
            return $headersCopy[$toLowerHeaderName];
        }
        return null;
    }

    /**
     * @return string
     */
    function getBody()
    {
        return $this->body;
    }

}