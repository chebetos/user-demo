<?php

namespace Chebetos\UserDemo\HTTP;


interface MessageInterface
{
    /**
     * @return array
     */
    function getHeaders();

    /**
     * @param $headerName
     * @return string
     */
    function getHeader($headerName);

    /**
     * @return string
     */
    function getBody();

}