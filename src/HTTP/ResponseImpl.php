<?php

namespace Chebetos\UserDemo\HTTP;


class ResponseImpl extends MessageImpl implements ResponseInterface, \JsonSerializable
{
    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusText;

    /**
     * @param string $headerName
     * @param string $headerValue
     */
    function setHeader($headerName, $headerValue)
    {
        if (null == $this->headers) {
            $this->headers = array();
        }
        $this->headers[$headerName] = $headerValue;
    }

    /**
     * @return int
     */
    function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    function getStatusText()
    {
        return $this->statusText;
    }

    /**
     * @param int $code
     * @param string $text
     */
    function setStatus($code, $text)
    {
        $this->statusCode = $code;
        $this->statusText = $text;
    }

    function setBody($content, $contentType = null)
    {
        $this->body = $content;
        if (!empty($contentType)) {
            $this->setHeader('Content-Type', $contentType);
        }
    }

    function sendHeaders()
    {
        if (headers_sent()) {
            return;
        }
        $headers = $this->getHeaders();
        if (!empty($headers)) {
            foreach ($headers as $name => $value) {
                header($name . ': ' . $value, false, $this->getStatusCode());
            }
        }
        header(sprintf('HTTP/1.0 %s %s', $this->getStatusCode(), $this->getStatusText()), true, $this->getStatusCode());
    }

    function send()
    {
        $this->sendHeaders();
        if (!empty($this->getBody()) && !empty(trim($this->getBody()))) {
            echo $this->getBody();
        }
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}