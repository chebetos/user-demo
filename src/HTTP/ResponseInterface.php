<?php

namespace Chebetos\UserDemo\HTTP;


interface ResponseInterface extends MessageInterface
{
    /**
     * @param string $headerName
     * @param string $headerValue
     */
    function setHeader($headerName, $headerValue);

    /**
     * @param int $code
     * @param string $text
     */
    function setStatus($code, $text);

    /**
     * @param string $content
     * @param string $contentType
     */
    function setBody($content, $contentType = null);

    /**
     * @return int
     */
    function getStatusCode();

    /**
     * @return string
     */
    function getStatusText();

    /**
     * @return void
     */
    function send();
}