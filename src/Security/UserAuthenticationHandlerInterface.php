<?php

namespace Chebetos\UserDemo\Security;

use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\Model\UserModel;

interface UserAuthenticationHandlerInterface
{
    /**
     * @param RequestInterface $request
     * @return UserModel
     */
    function getCurrentUser(RequestInterface $request);
}