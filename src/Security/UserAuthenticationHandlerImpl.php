<?php

namespace Chebetos\UserDemo\Security;


use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\Model\UserModel;
use Chebetos\UserDemo\Model\UserRepositoryInterface;

class UserAuthenticationHandlerImpl implements UserAuthenticationHandlerInterface
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $userRepo
     */
    function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * @param RequestInterface $request
     * @return UserModel
     */
    function getCurrentUser(RequestInterface $request)
    {
        $user = $request->getSessionVarValue('currentUser');
        if (!empty($user))
        {
            return $user;
        }

        $authUser = $request->getAuthUser();
        $authPW = $request->getAuthPassword();

        if (empty($authUser) || empty($authPW))
        {
            return null;
        }

        $user = $this->userRepository->getUser($authUser);
        if (empty($user))
        {
            return null;
        }
        
        $validPassword = $user->checkPassword($authPW);

        if (!$validPassword)
        {
            return null;
        }
        return $user;
    }
}