<?php

namespace Chebetos\UserDemo\Security;

use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\Route\RouteVO;

interface SecurityHandlerInterface
{
    const ANONYMOUS = 'ANONYMOUS';
    const LOGGED = 'LOGGED';

    const OK = 200;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;


    /**
     * @param RequestInterface $request
     * @param RouteVO $routeVO
     * @return bool
     */
    function userHasAuthorization(RequestInterface $request, RouteVO $routeVO);
}