<?php

namespace Chebetos\UserDemo\Security;

use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\Route\RouteVO;

class SecurityHandlerImpl implements SecurityHandlerInterface
{
    /**
     * @var UserAuthenticationHandlerInterface
     */
    private $userAuthenticationHandler;

    public function __construct(UserAuthenticationHandlerInterface $userAuthenticationHandler)
    {
        $this->userAuthenticationHandler = $userAuthenticationHandler;
    }

    function userHasAuthorization(RequestInterface $request, RouteVO $routeVO)
    {
        $authorizedRoles = $routeVO->getRoles();
        if (in_array(SecurityHandlerInterface::ANONYMOUS, $authorizedRoles))
        {
            return SecurityHandlerImpl::OK;
        }
        $user = $this->userAuthenticationHandler->getCurrentUser($request);
        if (empty($user))
        {
            return SecurityHandlerImpl::UNAUTHORIZED;
        }
        if (in_array(SecurityHandlerInterface::LOGGED, $authorizedRoles))
        {
            return SecurityHandlerImpl::OK;
        }
        $intersectRoles = array_intersect($authorizedRoles, $user->getRoles());
        if (!empty($intersectRoles))
        {
            return SecurityHandlerImpl::OK;
        }
        return SecurityHandlerImpl::FORBIDDEN;
    }
}