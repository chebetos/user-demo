<?php

namespace Chebetos\UserDemo\ViewRenderer;


interface ViewRendererInterface
{
    function renderPhpToString($file, array $vars = null);
}