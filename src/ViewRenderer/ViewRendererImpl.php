<?php

namespace Chebetos\UserDemo\ViewRenderer;


class ViewRendererImpl implements ViewRendererInterface
{
    /**
     * @param $file
     * @param null $vars
     * @return string
     */
    function renderPhpToString($file, array $vars = null)
    {
        if (is_array($vars) && !empty($vars)) {
            extract($vars);
        }
        ob_start();
        include $file;
        return ob_get_clean();
    }
}