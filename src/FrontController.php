<?php

namespace Chebetos\UserDemo;


use Chebetos\UserDemo\HTTP\RequestInterface;
use Chebetos\UserDemo\HTTP\ResponseInterface;
use Chebetos\UserDemo\Security\SecurityHandlerInterface;
use FastRoute\Dispatcher;
use Psr\Log\LoggerInterface;

/**
 * Controlador frontal de la aplicación
 */
class FrontController
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var SecurityHandlerInterface
     */
    private $securityHandler;

    /**
     * FrontController constructor.
     * @param Dispatcher $dispatcherP
     * @param SecurityHandlerInterface $securityHandler
     */
    public function __construct(Dispatcher $dispatcherP, SecurityHandlerInterface $securityHandler)
    {
        $this->dispatcher = $dispatcherP;
        $this->securityHandler = $securityHandler;
        $this->logger = Container::get('logger');
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     */
    public function handleRequest(RequestInterface $request, ResponseInterface $response)
    {
        //$this->logger->info("RECEIVED REQUEST: ", [ $request->getHttpMethod(), $request->getURI() ]);
        $routeInfo = $this->dispatcher->dispatch($request->getHttpMethod(), $request->getURI());
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $response->setStatus(404, 'NOT FOUND');
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                $response->setStatus(405, 'METHOD NOT ALLOWED');
                $response->setHeader('Allow', implode(',', $allowedMethods));
                break;
            case Dispatcher::FOUND:
                $routeVO = $routeInfo[1];
                $vars = $routeInfo[2];

                if (empty($vars))
                {
                    $vars = array();
                }

                $hasAuth = $this->securityHandler->userHasAuthorization($request, $routeVO);
                switch ($hasAuth) {
                    case SecurityHandlerInterface::FORBIDDEN:
                        $response->setStatus(SecurityHandlerInterface::FORBIDDEN, 'FORBIDDEN');
                        return;
                    case SecurityHandlerInterface::UNAUTHORIZED:
                        $response->setStatus(SecurityHandlerInterface::UNAUTHORIZED, 'UNAUTHORIZED');
                        return;
                }

                //TODO Validar request body --> que el content type corresponda al body, y solo si es PUT o POST
                //TODO USER CONTROLLER QUE VALIDE que el USER tiene todos los datos
                //TODO Responder segun accept
                //TODO Soportar XML

                $params = array_merge( [$request, $response], [$vars]);

                $handler = $routeVO->getHandler();

                $controller = Container::get($handler[0]);
                $controllerCallable = array($controller, $handler[1]);

                //$this->logger->info("Invocando a: ", $controllerCallable);
                //$this->logger->info("\tParametros: ", $vars);

                call_user_func_array($controllerCallable, $params);

                //$response->setStatus(200, 'OK');
                //$response->setBody(json_encode($routeInfo));
                break;
        }
        //$response->setBody(json_encode($routeInfo));
        //$this->logger->info("RESPONSE: ", [ json_encode($routeInfo)]); // , json_encode($response)
    }
}