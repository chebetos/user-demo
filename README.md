# Chebetos User Demo

Aplicación PHP en la que se demuestra como implementar el patrón de MVC aplicando programación orientada a objetos (POO).
Se basa en los requerimientos establecidos en [REQUIREMENTS.md](REQUIREMENTS.md). 

![GitLab CI Build Status](http://gitlab.com/chebetos/user-demo/badges/master/build.svg)

## Características del Proyecto
* Gestión de dependencias con Composer.
* Inspección y generación automatizada de métricas de calidad.
* Pruebas automatizadas PHPUnit
* Automatización de la construcción (ejecución de pruebas, inspección y generación de métricas) con Phing (http://www.phing.info).
* Integración Contínua con GitLab CI (https://about.gitlab.com/gitlab-ci/)

## Comandos útiles
### Preparar entorno de desarrollo
* Ajustar timezone en php.ini a una zona horaria específica como `Europe/Madrid` (en la sección `[Date]`, la clave: `date.timezone`)
* Instalar xdebug: en Windows instrucciones en http://xdebug.org/wizard.php, en Ubuntu es `apt-get install php5-xdebug php5-xsl php5-curl` (las otras suelen ya estar activadas).
* Activar extensiones: php_openssl, php_curl, php_fileinfo, php_mbstring; en Windows es descomentar ciertas líneas en el fichero php.ini.
* Instalar composer (instrucciones en https://getcomposer.org/download/)
* Clonar proyecto y ejecutar `composer install`
* Opcional: Instalar docker (instrucciones en https://docs.docker.com/installation/windows/ y https://docs.docker.com/installation/ubuntulinux/)

### Construcción, inspección y pruebas
* Construcción completa: `vendor/bin/phing`
* Pruebas automatizadas: `vendor/bin/phpunit`
* Ejecutar prueba específica:  `vendor/bin/phpunit --filter [nombre de método de test]`
 
